# University Food Review Project

The University Food Review Project is a WeChat mini-program developed agilely with uni-app and ThinkPHP. It aims to solve the widespread "dining choice paralysis" problem among many students. By providing a platform for university students to review food vendors and cafeterias, it enables them to understand objective evaluations from fellow university students about certain dishes and establishments.

## User Guide

### Functional Description

> The main features are only briefly described here; refer to the mini-program for more details.

#### Users

1. Browse, search, and categorize to view merchant information
   - On the homepage, display information about all merchants and cafeterias.
   - Choose different schools' merchants for viewing by clicking on the upper-left corner of the school selection.
   - View merchants of different categories by choosing the respective category.
   - Search for merchants and dishes in the search box.
2. Comment on dishes, view, like, and reply to reviews
   - Rate and review dishes on the dish page.
   - Like and reply to other people's reviews.
3. Follow users, collect merchants
   - Receive updates on the "Trends" page after following other users.
   - Be notified through system messages when a collected merchant posts an activity.
4. Friends and chat
   - Add users as friends and engage in chat.
   - Chat with merchants without needing to add them as friends.
5. Report and appeal
   - Users can report comments or other users.
   - Comments will be deleted after a report is accepted, and users will be banned.
   - Banned users may appeal the decision.

#### Merchants

1. Edit personal information, dishes, and events
   - Merchants can add and manage their personal information, dishes, and events on their personal homepage.
2. Chat
   - Merchants can receive messages sent to them by others.

#### Administrators

Note: In the current version, users can select to become administrators during registration, but this entry point will be closed upon official release.

1. Handle appeals and reports
   - Process reports and appeals from other users based on submitted materials.
2. Manage cafeterias
   - Create new cafeterias and add dishes for them.
   - Receive, review, and edit dish update requests.
